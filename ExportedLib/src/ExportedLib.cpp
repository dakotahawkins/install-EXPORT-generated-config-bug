#include "ExportedLib.h"

#include <iostream>

namespace exported_lib
{
    void SayHello()
    {
        std::cout << "Hello, World!\n";
    }
}
