#!/bin/bash

scriptdir=$(dirname "$(readlink -f "$0")")

main() {
    echo "----------------------------------------------------------------------"
    echo "Configuring ExportedLib with default install directories, everything"
    echo "should work..."
    echo

    run_cmake -DCMAKE_INSTALL_PREFIX=../install

    echo
    echo "Worked as expected..."
    echo "----------------------------------------------------------------------"
    echo
    echo "======================================================================"
    echo
    read -n 1 -s -r -p "Press any key to continue and run the failure case..."
    echo
    echo
    echo "======================================================================"
    echo

    for project in ExportedLib DependsOnExportedLib; do
        for directory in build install; do
            backup_directory=${directory}-backup
            if [[ -d ${project}/${backup_directory} ]]; then
                rm -rf ${project}/${backup_directory} || {
                    error_exit "Failed to remove old backup: ${project}/${backup_directory}"
                }
            fi

            if [[ -d ${project}/${directory} ]]; then
                echo "Moving ${project}/${directory} to ${project}/${backup_directory}"
                mv ${project}/${directory} ${project}/${backup_directory} || {
                    error_exit "Failed to move ${project}/${directory} to ${project}/${backup_directory}"
                }
            fi
        done
    done

    echo
    echo "======================================================================"
    echo "======================================================================"
    echo "======================================================================"
    echo
    echo "----------------------------------------------------------------------"
    echo "Configuring ExportedLib with ../install directories. This will fail."
    echo

    run_cmake -DCMAKE_INSTALL_PREFIX=install \
              -DCMAKE_INSTALL_LIBDIR=../../install/lib \
              -DCMAKE_INSTALL_BINDIR=../../install/bin \
              -DCMAKE_INSTALL_INCLUDEDIR=../../install/include \
              -DINSTALL_CONFIGDIR:PATH=../../install/cmake

    echo
    echo "Didn't expect to get here..."
    echo "----------------------------------------------------------------------"
    echo
}

run_cmake() {
    cd "$scriptdir" || {
        error_exit "Failed to cd to scripts directory."
    }

    cd ExportedLib || {
        error_exit "Failed to cd to ExportedLib directory."
    }

    mkdir -p ./build || {
        error_exit "Failed to create ExportedLib build directory."
    }

    cd ./build || {
        error_exit "Failed to cd to ExportedLib build directory."
    }

    echo
    echo "-----------------------------------"
    echo "ExportedLib: generate"
    echo "stdout > ExportedLib/build/_generate_log.txt"
    echo "-----------------------------------"
    echo
    cmake "$@" .. > _generate_log.txt || {
        error_exit "ExportedLib cmake generation failed."
    }

    echo
    echo "-----------------------------------"
    echo "ExportedLib: build/install"
    echo "stdout > ExportedLib/build/_build_log.txt"
    echo "-----------------------------------"
    echo
    cmake --build . --clean-first --config Release --target install > _build_log.txt || {
        error_exit "Failed to build and install ExportedLib."
    }

    cd "$scriptdir" || {
        error_exit "Failed to cd to scripts directory."
    }

    cd DependsOnExportedLib || {
        error_exit "Failed to cd to DependsOnExportedLib directory."
    }

    mkdir -p ./build || {
        error_exit "Failed to create DependsOnExportedLib build directory."
    }

    cd ./build || {
        error_exit "Failed to cd to DependsOnExportedLib build directory."
    }

    echo
    echo "-----------------------------------"
    echo "DependsOnExportedLib: generate"
    echo "stdout > DependsOnExportedLib/build/_generate_log.txt"
    echo "-----------------------------------"
    echo
    cmake .. > _generate_log.txt || {
        error_exit "DependsOnExportedLib cmake generation failed."
    }

    echo
    echo "-----------------------------------"
    echo "DependsOnExportedLib: build"
    echo "stdout > DependsOnExportedLib/build/_build_log.txt"
    echo "-----------------------------------"
    echo
    cmake --build . --clean-first --config Release > _build_log.txt || {
        error_exit "Failed to build DependsOnExportedLib."
    }

    echo
    echo "-----------------------------------"
    echo "DependsOnExportedLib: test"
    echo "-----------------------------------"
    echo
    ctest -C Release || {
        error_exit "Unit test failed."
    }

    cd "$scriptdir" || {
        error_exit "Failed to cd to scripts directory."
    }
}

error_exit() {
    echo
    echo "Error: $@"
    echo "----------------------------------------------------------------------"
    echo
    exit 1
}

main "$@"
exit 0
